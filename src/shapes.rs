use super::{gen_builder_ops, Builder};

/// ## Shape trait
trait Shape: Builder + Clone {}

/// ## Cone
#[derive(Clone)]
pub struct Cone {
    bottom_radius: f32,
    top_radius: f32,
    height: f32,
    center_z: bool,
    nr_fragments: usize,
}

impl Cone {
    pub fn new(bottom_radius: f32, top_radius: f32, height: f32) -> Self {
        Self {
            bottom_radius,
            top_radius,
            height,
            center_z: false,
            nr_fragments: 0,
        }
    }
}

impl Builder for Cone {
    fn build(self) -> String {
        format!(
            "cylinder({}, {}, {}, {}, $fn={});",
            self.height, self.bottom_radius, self.top_radius, self.center_z, self.nr_fragments
        )
    }
}

/// ## Cylinder
#[derive(Clone)]
pub struct Cylinder(Cone);
impl Cylinder {
    pub fn new(radius: f32, height: f32) -> Self {
        Self(Cone::new(radius, radius, height))
    }

    pub fn set_resolution(self, resolution: usize) -> Self {
        Cylinder(self.0.set_resolution(resolution))
    }
}

impl Builder for Cylinder {
    fn build(self) -> String {
        self.0.build()
    }
}

/// ## Sphere
#[derive(Clone)]
pub struct Sphere {
    radius: f32,
    nr_fragments: usize,
}

impl Sphere {
    pub fn new(radius: f32) -> Self {
        Self {
            radius,
            nr_fragments: 0,
        }
    }
}

impl Builder for Sphere {
    fn build(self) -> String {
        format!("sphere({}, $fn={});", self.radius, self.nr_fragments)
    }
}

/// ## A Rectangular Cuboid
#[derive(Clone)]
pub struct Cuboid {
    width: f32,
    depth: f32,
    height: f32,
    center: bool,
}

impl Cuboid {
    pub fn new(width: f32, depth: f32, height: f32) -> Self {
        Self {
            width,
            depth,
            height,
            center: false,
        }
    }
}

impl Builder for Cuboid {
    fn build(self) -> String {
        format!(
            "cube([{},{},{}], center={});",
            self.width, self.depth, self.height, self.center
        )
    }
}

/// ## A Cube
#[derive(Clone)]
pub struct Cube(Cuboid);
impl Cube {
    pub fn new(size: f32) -> Self {
        Self(Cuboid::new(size, size, size))
    }
}

impl Builder for Cube {
    fn build(self) -> String {
        self.0.build()
    }
}

/// ## Polyhedron
// TODO: Abstractions to make this more usable
#[derive(Clone)]
pub struct Polyhedron {
    points: Vec<(f32, f32, f32)>,
    faces: Vec<Vec<usize>>,
    convexity: u32,
}

impl Polyhedron {
    pub fn new(points: Vec<(f32, f32, f32)>, faces: Vec<Vec<usize>>) -> Self {
        Self {
            points,
            faces,
            convexity: 1,
        }
    }
}

impl Builder for Polyhedron {
    fn build(self) -> String {
        let points = "[".to_string()
            + &self
                .points
                .iter()
                .map(|(x, y, z)| format!("[{x},{y},{z}]"))
                .collect::<Vec<String>>()
                .join(",")
            + "]";
        let faces = "[".to_string()
            + &self
                .faces
                .iter()
                .map(|face| {
                    "[".to_string()
                        + &face
                            .iter()
                            .map(usize::to_string)
                            .collect::<Vec<String>>()
                            .join(",")
                        + "]"
                })
                .collect::<Vec<String>>()
                .join(",")
            + "]";
        format!(
            "polyhedron({points}, {faces}, convexity={});",
            self.convexity
        )
    }
}

/// # Macro for defining Shape, Add and Sub traits
macro_rules! gen_shape_traits {
    ($T:ty) => {
        gen_builder_ops!($T);
        impl Shape for $T {}
    };
}

// Define traits on types
gen_shape_traits!(Cone);
gen_shape_traits!(Cylinder);
gen_shape_traits!(Sphere);
gen_shape_traits!(Cuboid);
gen_shape_traits!(Cube);
gen_shape_traits!(Polyhedron);

/// # Macro for defining objects with circles
macro_rules! gen_circle_fns {
    ($T:ty) => {
        impl $T {
            pub fn set_resolution(mut self, resolution: usize) -> Self {
                if resolution > 128 {
                    println!(
                        "Resolution {resolution} of circle \
                                is above maximum value recommended by OpenSCAD (128)"
                    );
                }
                self.nr_fragments = resolution;
                self
            }
        }
    };
}
gen_circle_fns!(Cone);
gen_circle_fns!(Sphere);

#[cfg(test)]
mod test {
    // Tests are based on wiki examples:
    // https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Primitive_Solids
    use super::*;

    #[test]
    fn test_cube() {
        let cube = Cube::new(1.0);
        let expected = "cube([1,1,1], center=false);";
        assert_eq!(cube.build(), expected);
    }

    #[test]
    fn test_cuboid() {
        let cuboid = Cuboid::new(1.0, 2.0, 3.0);
        let expected = "cube([1,2,3], center=false);";
        assert_eq!(cuboid.build(), expected);
    }

    #[test]
    fn test_sphere() {
        let sphere = Sphere::new(1.0);
        let expected = "sphere(1, $fn=0);";
        assert_eq!(sphere.clone().build(), expected);
        let sphere = sphere.set_resolution(32);
        let expected = "sphere(1, $fn=32);";
        assert_eq!(sphere.build(), expected);
    }

    #[test]
    fn test_cylinder() {
        let cylinder = Cylinder::new(4.0, 2.0);
        let expected = "cylinder(2, 4, 4, false, $fn=0);";
        assert_eq!(cylinder.clone().build(), expected);
        let cylinder = cylinder.set_resolution(32);
        let expected = "cylinder(2, 4, 4, false, $fn=32);";
        assert_eq!(cylinder.build(), expected);
    }

    #[test]
    fn test_cone() {
        let cone = Cone::new(4.0, 2.0, 1.0);
        let expected = "cylinder(1, 4, 2, false, $fn=0);";
        assert_eq!(cone.clone().build(), expected);
        let cone = cone.set_resolution(32);
        let expected = "cylinder(1, 4, 2, false, $fn=32);";
        assert_eq!(cone.build(), expected);
    }

    #[test]
    fn test_polyhedron() {
        let points = vec![
            (0.0, 0.0, 0.0),  //0
            (10.0, 0.0, 0.0), //1
            (10.0, 7.0, 0.0), //2
            (0.0, 7.0, 0.0),  //3
            (0.0, 0.0, 5.0),  //4
            (10.0, 0.0, 5.0), //5
            (10.0, 7.0, 5.0), //6
            (0.0, 7.0, 5.0),  //7
        ];
        let faces = vec![
            vec![0, 1, 2, 3], // bottom
            vec![4, 5, 1, 0], // front
            vec![7, 6, 5, 4], // top
            vec![5, 6, 2, 1], // right
            vec![6, 7, 3, 2], // back
            vec![7, 4, 0, 3], // left
        ];
        let poly = Polyhedron::new(points, faces);
        let expected = "polyhedron([[0,0,0],[10,0,0],[10,7,0],[0,7,0],\
                                    [0,0,5],[10,0,5],[10,7,5],[0,7,5]], \
                                   [[0,1,2,3],[4,5,1,0],[7,6,5,4],\
                                    [5,6,2,1],[6,7,3,2],[7,4,0,3]], \
                                   convexity=1);";
        assert_eq!(poly.build(), expected)
    }
}
