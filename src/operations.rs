use super::{gen_builder_ops, Builder};

/// # Operations
/// ## Union
pub struct Union<A: Builder, B: Builder> {
    pub(super) first: A,
    pub(super) second: B,
}

impl<A: Builder, B: Builder> Builder for Union<A, B> {
    fn build(self) -> String {
        format!("{} {}", self.first.build(), self.second.build())
    }
}

/// ## Difference
pub struct Difference<A: Builder, B: Builder> {
    pub(super) first: A,
    pub(super) second: B,
}

impl<A: Builder, B: Builder> Builder for Difference<A, B> {
    fn build(self) -> String {
        format!(
            "difference(){{{} {}}}",
            self.first.build(),
            self.second.build()
        )
    }
}

/// ## Intersection
pub struct Intersection<A: Builder, B: Builder> {
    pub(super) first: A,
    pub(super) second: B,
}

impl<A: Builder, B: Builder> Builder for Intersection<A, B> {
    fn build(self) -> String {
        format!(
            "intersection(){{{} {}}}",
            self.first.build(),
            self.second.build()
        )
    }
}

/// ## Translation
pub struct Translation<A: Builder> {
    pub(super) x: f32,
    pub(super) y: f32,
    pub(super) z: f32,
    pub(super) object: A,
}

impl<A: Builder> Builder for Translation<A> {
    fn build(self) -> String {
        format!(
            "translate([{},{},{}]){{{}}}",
            self.x,
            self.y,
            self.z,
            self.object.build()
        )
    }
}

/// ## Rotation
pub struct Rotation<A: Builder> {
    pub(super) degrees: (f32, f32, f32),
    pub(super) object: A,
}

impl<A: Builder> Builder for Rotation<A> {
    fn build(self) -> String {
        format!(
            "rotate([{},{},{}]){{{}}}",
            self.degrees.0,
            self.degrees.1,
            self.degrees.2,
            self.object.build(),
        )
    }
}

gen_builder_ops!(Union<A1,A2>, A1, A2);
gen_builder_ops!(Difference<A1,A2>, A1, A2);
gen_builder_ops!(Intersection<A1,A2>, A1, A2);
gen_builder_ops!(Translation<A>, A);
gen_builder_ops!(Rotation<A>, A);
