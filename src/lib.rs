pub mod operations;
pub mod shapes;

pub mod prelude {
    pub use crate::operations::*;
    pub use crate::shapes::*;
    pub use crate::Builder;
}

use crate::operations::*;

/// # Builder Trait
type Coordinates = (f32, f32, f32);
pub trait Builder: Sized {
    fn build(self) -> String;
    fn union<B: Builder>(self, other: B) -> Union<Self, B> {
        Union {
            first: self,
            second: other,
        }
    }
    fn difference<B: Builder>(self, other: B) -> Difference<Self, B> {
        Difference {
            first: self,
            second: other,
        }
    }
    fn intersect<B: Builder>(self, other: B) -> Intersection<Self, B> {
        Intersection {
            first: self,
            second: other,
        }
    }
    fn translate(self, (x, y, z): Coordinates) -> Translation<Self> {
        Translation {
            x,
            y,
            z,
            object: self,
        }
    }
    fn rotate(self, degrees: Coordinates) -> Rotation<Self> {
        Rotation {
            degrees,
            object: self,
        }
    }
}

/// Macro for defining std::ops::Add and std::ops::Sub
#[macro_export]
macro_rules! gen_builder_ops {
    ($T:ty, $($A:ident),*) => {
        impl<B: Builder, $($A: Builder),*> std::ops::Add<B> for $T {
            type Output = $crate::operations::Union<Self, B>;
            fn add(self, other: B) -> Self::Output {
                self.union(other)
            }
        }

        impl<B: Builder, $($A: Builder),*> std::ops::Sub<B> for $T {
            type Output = $crate::operations::Difference<Self, B>;
            fn sub(self, other: B) -> Self::Output {
                self.difference(other)
            }
        }
    };
    ($T:ty) => {
        gen_builder_ops!($T, );
    };
}

/// Unit struct, useful for e.g. folding operations
#[derive(Default)]
pub struct Unit();
impl Builder for Unit {
    fn build(self) -> String {
        String::default()
    }
}
gen_builder_ops!(Unit);

#[cfg(test)]
mod test {
    use super::prelude::*;

    #[test]
    fn test_union() {
        let shape1 = Cube::new(2.0);
        let shape2 = Sphere::new(1.0);
        let shape = shape1.union(shape2);
        let expected = "cube([2,2,2], center=false); sphere(1, $fn=0);";
        assert_eq!(shape.build(), expected);
    }

    #[test]
    fn test_add() {
        let shape1 = Cube::new(2.0);
        let shape2 = Sphere::new(1.0);
        let shape = shape1 + shape2;
        let expected = "cube([2,2,2], center=false); sphere(1, $fn=0);";
        assert_eq!(shape.build(), expected);
    }

    #[test]
    fn test_difference() {
        let shape1 = Cube::new(2.0);
        let shape2 = Sphere::new(1.0);
        let shape = shape1.difference(shape2);
        let expected = "difference(){cube([2,2,2], center=false); sphere(1, $fn=0);}";
        assert_eq!(shape.build(), expected);
    }

    #[test]
    fn test_sub() {
        let shape1 = Cube::new(2.0);
        let shape2 = Sphere::new(1.0);
        let shape = shape1 - shape2;
        let expected = "difference(){cube([2,2,2], center=false); sphere(1, $fn=0);}";
        assert_eq!(shape.build(), expected);
    }

    #[test]
    fn test_intersection() {
        let shape1 = Cube::new(2.0);
        let shape2 = Sphere::new(1.0);
        let shape = shape1.intersect(shape2);
        let expected = "intersection(){cube([2,2,2], center=false); sphere(1, $fn=0);}";
        assert_eq!(shape.build(), expected);
    }

    #[test]
    fn test_translation() {
        let shape = Cube::new(2.0).translate((30.0, 30.0, 30.0));
        let expected = "translate([30,30,30]){cube([2,2,2], center=false);}";
        assert_eq!(shape.build(), expected);
    }

    #[test]
    fn test_rotation() {
        let shape = Cube::new(2.0).rotate((30.0, 30.0, 30.0));
        let expected = "rotate([30,30,30]){cube([2,2,2], center=false);}";
        assert_eq!(shape.build(), expected);
    }
}
