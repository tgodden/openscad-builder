use openscad_builder::prelude::*;

fn main() {
    let shape = Cylinder::new(4.0, 5.0).translate((1.0, 2.0, 3.0));
    println!("{}", shape.build());
}
