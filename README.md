# OpenSCAD Builder
A Rust library that allows you to generate OpenSCAD code from shapes defined in Rust.

This library uses the builder pattern to create and combine shapes. Unlike in OpenSCAD, shapes can be assigned to variables and cloned.

## Why I built this
OpenSCAD is a useful tool, but it's not easy to work with. I have two main issues with OpenSCAD:

- You can't assign shapes to a variable
- Arguments aren't always logically or consistently ordered

This is an attempt to solve these issues.
